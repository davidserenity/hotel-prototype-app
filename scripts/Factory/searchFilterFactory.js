module.factory('searchFilterFactory', function() {
 
  var searchfactory = function() {
    var propertyType = "sale";
    var data = {
        sale: {
          Bathrooms : "",
          Bedrooms : "",
          Locations : "",
          Carspaces : "",
          Price : "",
          PropertyType : "",
          Sort : ""
        },
        rent: {
          Bathrooms : "",
          Bedrooms : "",
          Locations : "",
          Carspaces : "",
          Price : "",
          PropertyType : "",
          Sort : ""
        }
    };
    return {
      setType: function(type){
        propertyType = type.trim();
      },
      getType: function(){
        return propertyType;
      },
      resetData: function(){
        for (var item in data[propertyType]) {
          data[propertyType][item] = "";
        }
        $('#'+propertyType+'bathroomLabel').text("Any");
        $('#'+propertyType+'bedroomLabel').text("Any");
        $('#'+propertyType+'carspacesLabel').text("Any");
        $('#'+propertyType+'priceLabel').text("Any");
        $('#'+propertyType+'sortLabel').text("Any");
        $('#'+propertyType+'locationsLabel').text("Any");
        $('#'+propertyType+'propertyTypeLabel').text("Any");
      },
      setData: function(searchFilter, value){
        data[propertyType][searchFilter] = value;      
      },
      getData: function(searchFilter){
        return data[propertyType][searchFilter];
      },
      getAll: function(){
        var sample = data[propertyType];
        return sample;
      }
    }    

  }();

  return searchfactory;
});