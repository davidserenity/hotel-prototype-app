module.factory('favouriteFactory', function() {

  var favfactory = function() {
    var data = [];
    var detailPageIndex = {};  
    return {
      resetData: function(){
        data = [];
      },
      setData: function(info){
        data.push(info);      
      },
      getData: function(index){
        return data[index];
      },
      setClass: function(id, aclass){
        for(x in data) {
          if(data[x]['id'] == id) {
            data[x]['class'] = aclass;
          }
        }
      },
      getAll: function(){
        var sample = data;
        return sample;
      },
      dataLength: function(){
        return data.length;
      },
      setDetailIndex: function(index){
        detailPageIndex['id'] = index;
      }
    }    

  }();

  return favfactory;
});