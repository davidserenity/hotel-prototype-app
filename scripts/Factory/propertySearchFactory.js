module.factory('propertyFactory', function() {

  var pfactory = function() {
    var data = [

/*{"name": "Ug 200M ",
     "procesor": "House in Ntinda",
     "image": 'defaultImage.png',
"id": 1,
"Car": 1
   },
    {"name": "Ug 80M",
     "procesor": "House for sale Kiwatule",
     "id": 2,
     "image": 'defaultImage.png'},
    {"name": "Ug 500k",
     "procesor": "House for rent Kyanja",
     "id": 3,
     "image": 'defaultImage.png'},
    {"name": "Ug 800M",
     "procesor": "Mansion in Munyonyo",
     "id": 4,
     "image": 'defaultImage.png'},
    {"name": "Ug 600M",
     "procesor": "Shop for sale",
     "id": 5,
     "image": 'mga_1_674.jpeg'},
    {"name": "Ug 400M",
     "procesor": "Bar for sale in Wandegeya",
     "id": 6,
     "image": 'munyonyo.jpeg'},
    {"name": "Ug 290M",
     "procesor": "4 Bed roomed house for sale in Muyenga",
     "id": 7,
     "image": 'naguru2.jpeg'},
    {"name": "Ug 430M",
     "procesor": "Mansion in Kyanja",
     "id": 8,
     "image": 'bugolobi4.jpeg'},
    {"name": "Ug 300K",
     "procesor": "2 bedrooms house for rent in Mukono",
     "id": 9,
     "image": 'makindye2.jpeg'},
    {"name": "Ug 800k",
     "procesor": "Apartment in namugongo for rent",
     "id": 10,
     "image": 'buwate.jpeg'},
    {"name": "Ug 500k",
     "procesor": "3 bedrooms house in Najera",
     "id": 11,
     "image": 'naguru3.jpeg'},
    {"name": "Ug 250M",
     "procesor": "Detached Houses in Mpererwe",
     "id": 12,
     "image": 'makindye.jpeg'}*/
  ];
    var detailPageIndex = {};  
    
    return {
      resetData: function(){
        data = [];
      },
      setData: function(info){
        data.push(info);      
      },
      getData: function(index){
        return data[index];
      },
      setClass: function(id, aclass){
        for(x in data) {
          if(data[x]['id'] == id) {
            data[x]['class'] = aclass;
          }
        }
      },
      getAll: function(){
        var sample = data;
        return sample;
      },
      dataLength: function(){
        return data.length;
      },
      setDetailIndex: function(index){
        detailPageIndex['id'] = index;
      }
    }    

  }();
   
  return pfactory;
});