module.factory('checkmeoutFactory', function() {

  var pfactory = function() {
    var data = [

    {"name": "Jenny",
     "procesor": "Ntinda",
     "image": 'yello.jpg',
     "id": 1,
     "Car": 1,
     "time": '7am-11pm'
    },
    {"name": "Maira",
     "procesor": "Kiwatule",
     "id": 2,
     "time": '8am-11pm',
     "image": 'yello (1).png'},
    {"name": "Sophie",
     "procesor": "Kyanja",
     "id": 3,
     "time": '10am-10pm',
     "image": 'yello (2).jpg'},
    {"name": "Rose",
     "procesor": "Munyonyo",
     "id": 4,
     "time": '8am-10pm',
     "image": 'yello (3).jpg'},
    {"name": "Sarah",
     "procesor": "Nakawa",
     "id": 5,
     "time": '10am-10pm',
     "image": 'yello (4).jpg'},
    {"name": "Clare",
     "procesor": "Wandegeya",
     "id": 6,
     "time": '8am-6pm',
     "image": 'yello (5).jpg'},
    {"name": "Carol",
     "procesor": "Muyenga",
     "id": 7,
     "time": '8am-10pm',
     "image": 'yello (6).jpg'}
  ];
    var detailPageIndex = {};  
    
    return {
      resetData: function(){
        data = [];
      },
      setData: function(info){
        data.push(info);      
      },
      getData: function(index){
        return data[index];
      },
      getAll: function(){
        var sample = data;
        return sample;
      },
      dataLength: function(){
        return data.length;
      },
      setDetailIndex: function(index){
        detailPageIndex['id'] = index;
      },
      getDetailIndex: function(){
        return detailPageIndex['id'];
      }
    }    

  }();
   
  return pfactory;
});