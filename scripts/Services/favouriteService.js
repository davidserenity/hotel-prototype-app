
(function ($) {
  module.service(
      "favouriteService",
      function( $q, databaseService, propertyFactory, favouriteFactory ) {
        // Return public API.
        return({
          getDelegate: getDelegate,
          toggleStar: toggleStar
        });
        // ---
        // PRIVATE METHODS.
        // ---
        function getDelegate() {
          var d = $.Deferred();
          initFromDb().then(function(){
            d.resolve();
          });
          return d;
        }

        // Change star state and save or delete property from dB
        function toggleStar($event, id, property, index) {
          $event.stopPropagation();
          var elementClass = $event.currentTarget;
          if($(elementClass).hasClass('faved')){
            console.log("Un-favourited "+id);
            $(elementClass).removeClass("faved");

            databaseService.readAll("savedProperty").then(function(items){

              for(var x in items){
                if(items[x]['id'] == id){
                  databaseService.deleteRow("savedProperty", items[x]['id']).then(function(){
                    console.log("deleted item "+id);

                    //check if we are deleting the last item in the favourites page and move to the main search page to avoid
                    //inserting an undefined object  
                    if (property == favouriteFactory && property.dataLength() == 1) { myMenu.setMainPage('html/propertyList.html', {closeMenu: true});}
                    initFromDb();
                  });
                }
              }

            });

          }else{
            console.log("favourited "+id);
            $(elementClass).addClass("faved");

            //uncomment to save favourited property
            databaseService.insert("savedProperty", property.getData(index)).then(function(items){
              initFromDb();
            }).fail(function(){
            });  

          }
        }


        // Initialise favourite factory from database.
        function initFromDb() {
          var d = $.Deferred();

          favouriteFactory.resetData();
          databaseService.readAll("savedProperty").then(function(items){
            for(var x in items) {
              favouriteFactory.setData(items[x]);
            }
            d.resolve();
          }).fail(function(){
            d.reject();
          });
          return d;
        }
      }
  );

})(jQuery);