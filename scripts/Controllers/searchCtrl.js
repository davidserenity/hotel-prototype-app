(function ($) {
  module.controller('propertySearch', function ($scope, databaseService, propertyListService, searchFilterFactory, propertyFactory) {
    
    
    
    $scope.search = function() {
      console.log("Clicked");

      /////////////Uncomment after testing testData/////////////
      propertyListService.getProperties().then(function( saloons ) {
        if(saloons['data'].length > 0){
          console.log("Now searching ");
          propertyFactory.resetData(); 
          for(var index = 0; index < saloons['data'].length; index++) {
            if(saloons['data'][index]['image'].length == 0) {
              saloons['data'][index]['image'] = "lib/onsen/images/defaultImage.png";
            }

            saloons['data'][index]['class'] = '';
            propertyFactory.setData(saloons['data'][index]);  
          }
          
          $scope.$emit('showListresults');            

        }
      }, function() {
          console.log("Error getting properties");        
      });
    }

    //create string from search parameters to display below list title
    $scope.getSubtitleString = function(){
      var searchString = "";
      var searchMatrix = searchFilterFactory.getAll();
      for(var index in searchMatrix) {
        if (searchMatrix[index].length > 0) {
          searchString = index + "." + searchMatrix[index] + "." + searchString;        
        };
      }
      return searchString;
    }

    //Clear search values
    $scope.clear = function() {
      searchFilterFactory.resetData();
    }
    
    //Check internet connection
    $scope.checkInternet = function() {
      var d = $.Deferred();
      
      document.addEventListener("deviceready", function(){
        var networkState = navigator.connection.type;
        setTimeout(function(){
          networkState = navigator.connection.type;
          var states = {};
          states[Connection.UNKNOWN] = 'Unknown connection';
          states[Connection.ETHERNET] = 'Ethernet connection';
          states[Connection.WIFI] = 'WiFi connection';
          states[Connection.CELL_2G] = 'Cell 2G connection';
          states[Connection.CELL_3G] = 'Cell 3G connection';
          states[Connection.CELL_4G] = 'Cell 4G connection';
          states[Connection.CELL] = 'Cell generic connection';
          states[Connection.NONE] = 'No network connection';
          if ((states[networkState] == 'No network connection') || (states[networkState] == 'Unknown connection')) {
            d.reject();
          } else {
            d.resolve();
          }
        }, 1000);
      }, false);
      
      return d;
    }
    
    //move this to the property factory
    $scope.cities = [
                     {"name": "Jinja",
                       "procesor": "Intel i5",
                       "age": 2011},
                       {"name": "Wandegeya",
                         "procesor": "Intel i7",
                         "age": 2010},
                         {"name": "Kyanja",
                           "procesor": "Intel core 2 duo",
                           "age": 2008},
                           {"name": "Ntinda",
                             "procesor": "Intel core 2 duo",
                             "age": 2012},
                             {"name": "Kasubi",
                               "procesor": "AMD",
                               "age": 2006},
                               {"name": "Mulago",
                                 "procesor": "Intel i5",
                                 "age": 2009},
                                 {"name": "Kamocha",
                                   "procesor": "Intel i7",
                                   "age": 2008},
                                   {"name": "Bukoto",
                                     "procesor": "Intel i5",
                                     "age": 2011},
                                     {"name": "Kiwatule",
                                       "procesor": "Intel i7",
                                       "age": 2010},
                                       {"name": "Mengo",
                                         "procesor": "Intel core 2 duo",
                                         "age": 2008},
                                         {"name": "Lubiri",
                                           "procesor": "Intel core 2 duo",
                                           "age": 2012},
                                           {"name": "Bweyogerere",
                                             "procesor": "AMD",
                                             "age": 2006},
                                             {"name": "Kireka",
                                               "procesor": "Intel i5",
                                               "age": 2009},
                                               {"name": "Banda",
                                                 "procesor": "Intel i7",
                                                 "age": 2008},
                                                 {"name": "Lugazi",
                                                   "procesor": "Intel i5",
                                                   "age": 2011},
                                                   {"name": "Kyambogo",
                                                     "procesor": "Intel i7",
                                                     "age": 2010},
                                                     {"name": "Old Kampala",
                                                       "procesor": "Intel core 2 duo",
                                                       "age": 2008},
                                                       {"name": "Zana",
                                                         "procesor": "Intel core 2 duo",
                                                         "age": 2012},
                                                         {"name": "Najanankumbi",
                                                           "procesor": "AMD",
                                                           "age": 2006},
                                                           {"name": "Bunga",
                                                             "procesor": "Intel i5",
                                                             "age": 2009},
                                                             {"name": "Muyenga",
                                                               "procesor": "Intel i7",
                                                               "age": 2008}
                                                             ];
    $scope.orderList = "name";

    $scope.successFunction = function(position) {
      var lat = position.coords.latitude;
      var lng = position.coords.longitude;
      console.log(lat+" "+lng);
    }
    $scope.errorFunction = function(position) {
      alert("Geocoder failed");
    }

    //Start geolocation when location switch is toggled on
    $scope.change = function($event){
      if(mySwitch.isChecked()){
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition($scope.successFunction, $scope.errorFunction);
        } 
      }
    }

    //Set search parameter values for all filters
    $scope.set = function(search_param) {
      myNavigator.popPage();
      var bath , bed , car , price, loc , prop, rentOrsale, sorter;
      if(searchFilterFactory.getType() != "rent"){
        bath = $('#salebathroomLabel') , bed = $('#salebedroomLabel') , car = $('#salecarspacesLabel') , price = $('#salepriceLabel'), loc = $('#salelocationsLabel'), sorter = $('#salesortLabel') , prop = $('#salepropertyTypeLabel') , rentOrsale = 'sale';
      }else{
        bath = $('#rentbathroomLabel') , bed = $('#rentbedroomLabel') , car = $('#rentcarspacesLabel') , price = $('#rentpriceLabel'), loc = $('#rentlocationsLabel'), sorter = $('#rentsortLabel') , prop = $('#rentpropertyTypeLabel') , rentOrsale = 'rent';
      }
      if(search_param == 'bathrooms') {
        searchFilterFactory.setData('Bathrooms', $('.dpui-numberPicker-input').val());
        bath.text(searchFilterFactory.getData('Bathrooms'));
      }else if(search_param == 'bedrooms') {
        searchFilterFactory.setData('Bedrooms', $('.dpui-numberPicker-input').val());
        bed.text(searchFilterFactory.getData('Bedrooms'));
      }else if(search_param == 'carspaces') {
        searchFilterFactory.setData('Carspaces', $('.dpui-numberPicker-input').val());
        car.text(searchFilterFactory.getData('Carspaces'));
      }else if(search_param == 'price') {
        var priceRange = $('span.min').find('.dpui-numberPicker-decrease').next().val() + " to "+ $('span.max').find('.dpui-numberPicker-decrease').next().val();
        searchFilterFactory.setData('Price', priceRange); 
        price.text(searchFilterFactory.getData('Price'));
      }else if(search_param == 'locations') {
        /*var location = "Cu"
          if(mySwitch.isChecked()){
          }
        searchFilterFactory[rentOrsale][search_param] = "";
        loc.text(searchFilterFactory[rentOrsale][search_param]);*/
      }else if(search_param == 'propertyType') {
        var propertyTypes = {};
        var displayValue = "";
        $('.propertyTypeCheckboxes').find('input:checked').each(function(){
          propertyTypes[$(this).attr('class')] = $(this).attr('class');
          displayValue = displayValue + " " + $(this).attr('class');
        });
        searchFilterFactory.setData('PropertyType', displayValue);
        prop.text(displayValue);

      }else if(search_param == 'sort') {
        searchFilterFactory.setData('PropertyType', $('input[name=r]:checked').val());
        sorter.text(searchFilterFactory.getData('PropertyType'));
      }
    }

    $scope.spinner = dpUI;
    $scope.initPicker = function(param){
      $scope.spinner.numberPicker(param.id, {
        min: param.min,
        max: param.max,
        step: param.step,
        format: param.format,
        formatter: function(x){
          return param.prefix+" "+x+" "+param.suffix;
        }
      });  
    }
    var minPriceParam = {id: "#minprice", min: 100, max:500, step:50, format: false, prefix:"UG:", suffix:"K"};
    var maxPriceParam = {id: "#maxprice", min: 500, max:900, step:50, format: false, prefix:"UG:", suffix:"K +"};
    var bedroomParam = {id: "#bedrooms", min: 1, max: 5, step: 1, format: false, prefix:"", suffix:"+"};
    var bathroomParam = {id: "#bathrooms", min: 1, max: 5, step: 1, format: false, prefix:"", suffix:"+"};
    var carspacesParam = {id: "#carspaces", min: 1, max: 5, step: 1, format: false, prefix:"", suffix:"+"};
    $scope.initPicker(minPriceParam);
    $scope.initPicker(maxPriceParam);
    $scope.initPicker(bedroomParam);
    $scope.initPicker(bathroomParam);
    $scope.initPicker(carspacesParam);


    $scope.toggleClass = function(param) {
      if(param == "sale"){
        searchFilterFactory.setType("sale");
        $(".saleTab").addClass("activeTab");
        $(".rentTab").removeClass("activeTab");
      }else {
        searchFilterFactory.setType("rent");
        $(".rentTab").addClass("activeTab");
        $(".saleTab").removeClass("activeTab");
      }
    }

  });
} )(jQuery);