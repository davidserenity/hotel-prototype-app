/**
 * Manage Saved property listings
 */

(function ($) {
  module.controller('splash', function ($scope) {
      
    $scope.setTimer = function() {
      setTimeout(function(){ 
        navi.pushPage('imageSlides.html'); 
        
      }, 5000);
    }
    $scope.setTimer();
  });
} )(jQuery);
