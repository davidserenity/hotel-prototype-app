/**
 * Manage Saved property listings
 */

(function ($) {
  module.controller('savedProperty', function ($scope, $q, propertyFactory, favouriteService, favouriteFactory) {
    
    $scope.setDelegate = function(){

      $scope.MyFavouriteDelegate = {
            height : function(){
              if(window.screen.width <= 240){
                return 145;
              }else if(window.screen.width <= 320){
                return 230;
              }else if(window.screen.width <= 360){
                return 265;
              }else if(window.screen.width <= 375){
                return 280;
              }else if(window.screen.width <= 480){
                return 265;
              }

            },
            configureItemScope: function(index, itemScope) {

              if (!itemScope.item) {
                itemScope.canceler = $q.defer();
                itemScope.item = {
                    Name: 'Please wait',
                    City: 'Loading',
                    Distance: 'Property'
                };

                itemScope.item = favouriteFactory.getData(index); 
              }
            },

            calculateItemHeight: function(index) {
              return this.height();
            },

            countItems: function() {
              return favouriteFactory.dataLength();
            },

            destroyItemScope: function(index, itemScope) {
              itemScope.canceler.resolve();
            }
          };
    }    

    $scope.favourite = function($event, id, index) {
      var elementClass = $event.currentTarget;
      if(!$(elementClass).hasClass('faved')){
        favouriteFactory.setClass(id, 'faved');  
        propertyFactory.setClass(id, 'faved'); 
      }else{
        favouriteFactory.setClass(id, ''); 
        propertyFactory.setClass(id, '');   
      }
      favouriteService.toggleStar($event, id, favouriteFactory, index);
    }

    $scope.checkFavouritesLength = function(){
      if (favouriteFactory.dataLength() > 0) {
        $scope.showNoPropertiesFoundPage = false;
        $scope.showSavedProperties = true;
      }else{
        $scope.showNoPropertiesFoundPage = true;
        $scope.showSavedProperties = false;
      }
    }
    $scope.checkFavouritesLength();
    
    $scope.setParams = function(id) {
      favouriteFactory.setDetailIndex(id);
      favNavigator.pushPage("html/detailTemplate.html");
    }

    $scope.setDelegate();
  });
} )(jQuery);
