(function ($) {
  module.controller('detail', function ($scope) {
  	$('.datepicker').pickadate({
  		format: 'dd/mm/yy'
  	});
    $scope.loadGallery = function(){
      myNavigator.pushPage('html/imageSlides.html')
    }

    $scope.readmore = function(){
      myNavigator.pushPage('html/readMore.html');
    }

  });
} )(jQuery);
