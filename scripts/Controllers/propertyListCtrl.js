(function ($) {
  module.controller('propertyList', function ($scope, $q, favouriteFactory, databaseService, searchFilterFactory, propertyFactory, favouriteService) {
    
    $scope.subtitle = localStorage.subtitle;
    $scope.setDelegate = function(){
      $scope.MyDelegate = {
          height : function(){
            if(window.screen.width <= 240){
              return 145;
            }else if(window.screen.width <= 320){
              return 230;
            }else if(window.screen.width <= 360){
              return 265;
            }else if(window.screen.width <= 375){
              return 280;
            }else if(window.screen.width <= 480){
              return 265;
            }

          },
          configureItemScope: function(index, itemScope) {
            if (!itemScope.item) {
              itemScope.canceler = $q.defer();
              itemScope.item = {
                  name: 'Please Wait',
                  procesor: 'Loading Properties',
                  image: 'defaultImage.png',
                  show: false
              };

              itemScope.item = propertyFactory.getData(index);
            }
          },

          calculateItemHeight: function(index) {
            return this.height();
          },

          countItems: function() {
            return propertyFactory.dataLength();
          },

          destroyItemScope: function(index, itemScope) {
            itemScope.canceler.resolve();
          }
      };

    }

    //Check if we have properties in Property factory and either show
    //empty page or list of properties
    $scope.checkPropertyLength = function() {
      if (propertyFactory.dataLength() > 0) {
        $scope.showBlankPage = false;
        $scope.showListPage = true;

      }else{
        $scope.showBlankPage = true;
        $scope.showListPage = false;

      }
    }

    $scope.checkPropertyLength();
    $scope.setDelegate();
    favouriteService.getDelegate();

    //List property list results by setting delegate object
    $scope.$on('showListresults', function(event) {
      $scope.checkPropertyLength();
      $scope.subtitle = localStorage.subtitle;
      myNavigator.popPage();
      $scope.setDelegate();
    });

    //Toggle saved search menu state and save search parameters object
    $scope.menuState = {};
    $scope.menuState.show = true;
    $scope.toggle = function(){
      $scope.menuState.show = !$scope.menuState.show;
      databaseService.insert("savedSearch", $scope.getSearchObject());  
    }


    //Toggle sortmenu
    $scope.sortState = {};
    $scope.sortState.show = false;
    $scope.toggleSort = function(){
      $scope.sortState.show = !$scope.sortState.show;
      
    }

    //Create object with parameters for last search 
    $scope.getSearchObject = function(){
      var searchObject = {};
      
      for(var index in searchFilterFactory.getAll()) {
        if (searchFilterFactory.getData(index) != "") {
          searchObject[index] = searchFilterFactory.getData(index);        
        }
      }
      return searchObject;
    }

    //Set index of property clicked to be passed
    //onto the details page 
    $scope.setParams = function(id){
      propertyFactory.setDetailIndex(id);
      myNavigator.pushPage("html/detailTemplate.html");
    }

    //Add favourite class to white star making it yellow - favService
    //Also togge class name in data structure for property Factory
    $scope.favourite = function($event, id, index) {
      var elementClass = $event.currentTarget;
      if(!$(elementClass).hasClass('faved')){
        propertyFactory.setClass(id, 'faved');
         
      }else{
        propertyFactory.setClass(id, '');  
        
      }
      favouriteService.toggleStar($event, id, propertyFactory, index);
    }


  });
} )(jQuery);
