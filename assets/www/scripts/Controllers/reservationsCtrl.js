
(function ($) {
  module.controller('reservation', function ($scope) {
    $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  	var $input = $('.datepicker').pickadate({
  		format: 'dd/mm/yy',
      onClose: function() {
        console.log('Closed now '+this.get('id'));
        console.log('set values '+this.get('select'));
        var pickerObjt = this.get('select');
        $('.'+this.get('id')+'-month').html("<small>"+pickerObjt['year']+"</small>")
        $('.'+this.get('id')+'-date-year').html(pickerObjt['date']+" "+$scope.months[pickerObjt['month']])
        this.clear();
      },
      onSet: function(context) {
        console.log('Just set stuff:', context)

      }
  	});



  });
})(jQuery);
