module.controller('GalleryController', function($scope, $timeout){
        $scope.initPhotoswipe = function(x) {
			$timeout(function(){
				var pswpElement = document.querySelectorAll('.pswp')[0];

			// build items array
			var items = [
			{
				src: 'lib/onsen/images/new/swim1.jpg',
				w: 360,
				h: 238
			},
			{
				src: 'lib/onsen/images/new/swimming-pool-view-at-night.jpg',
				w: 360,
				h: 238
			},
			{
				src: 'lib/onsen/images/new/swimming-at-kabira.jpg',
				w: 360,
				h: 238
			},
			{
				src: 'lib/onsen/images/new/res4.jpg',
				w: 360,
				h: 238
			},
			{
				src: 'lib/onsen/images/new/res3.jpg',
				w: 360,
				h: 238
			},
			{
				src: 'lib/onsen/images/new/tennis.jpg',
				w: 360,
				h: 238
			}
			];

			// define options (if needed)
			var options = {
				// optionName: 'option value'
				// for example:
				index: 0 // start at first slide
			};

			// Initializes and opens PhotoSwipe
			var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
			gallery.init();
			gallery.listen('close', function() { 
			  myMenu.setMainPage('html/home.html', {closeMenu: true})
			});
			},500);
        };
		
		$scope.initPhotoswipe();

    });
