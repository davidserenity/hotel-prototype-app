(function ($) {
  module.controller('dine', function ($scope, slideFactory) {

    $scope.setAuto = function() {
      slideFactory.clear().then(function(){
        var count = 0;
        var intervalID = window.setInterval(function() {
          count = count + 1;
          if(count <= 4){
            diningslider.next();
          }else{
            diningslider.first();
            count = 0;
          }

        }, 2000);   
        slideFactory.set(intervalID); 
      });

    }
    $scope.setAuto();
        });
} )(jQuery);
